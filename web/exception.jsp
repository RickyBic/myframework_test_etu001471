<%
    Exception e = (Exception) request.getAttribute("Exception");
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h2>Exception Occured !</h2>
        <p style="color: red"><%out.print(e);%></p>
    </body>
</html>
