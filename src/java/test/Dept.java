package test;

import framework.ModelView;
import framework.Url;
import java.sql.Connection;

/**
 *
 * @author RickyBic
 */
public class Dept {

    private Integer id;
    private String libelle;

    public Dept() {

    }

    public Dept(String libelle) {
        this.libelle = libelle;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    @Url("deptList.do")
    public ModelView lister() throws Exception {
        Connection con = PostgreSQLCon.getConnection();
        Dept[] ld = null;
        try {
            Object[] ldo = GenericDAO.find(new Dept(), con);
            ld = new Dept[ldo.length];
            for (int i = 0; i < ldo.length; i++) {
                ld[i] = (Dept) ldo[i];
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (con != null) {
                con.close();
            }
        }
        ModelView mv = new ModelView();
        mv.setUrl("listeDept.jsp");
        mv.getData().put("ld", ld);
        return mv;
    }

    @Url("saveDept.do")
    public ModelView ajout() throws Exception {
        Connection con = PostgreSQLCon.getConnection();
        try {
            GenericDAO.save(this, con);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (con != null) {
                con.close();
            }
        }
        ModelView mv = new ModelView();
        mv.setUrl("deptList.do");
        return mv;
    }

}
